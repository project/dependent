//if (Drupal.jsEnabled) {
    var table= new Array();    
    var i=0;
    var parent_num=1;
    var table_count=0;
    var parent_forms=new Array(1);             
    $(document).ready(function(){  
    //debugger;    
      $("div.this_is_a_parent", "#node-form").each(function(){                         
          var field_hidden_text=$(this).html().split(' ')[0];     //the hidden text contains details needed to determine the children such as the field type name of the parent                                       
          var the_parent_form=$(this).prev();          
          $(the_parent_form).each(function(){                  
            var parent_form_item=get_prev_form_item($(this));     //get the real parent form-item the '.this_is_a_parent' element is after the form-item element                                                                  
            var has_attribute_parent_num=$(parent_form_item).attr('parent_num');
            if ((($(parent_form_item).attr('parent_num'))===null)||(typeof ($(parent_form_item).attr('parent_num'))=="undefined")){ //check if this dom element has been inserted to the parent_forms array
              $(parent_form_item).attr('parent_num',parent_num);  //set the parent number attribute                          
              parent_forms.push(new Array());                          
              parent_forms[parent_num]["children_arr"]=new Array();
              parent_forms[parent_num]["parents_arr"]=new Array();
              parent_forms[parent_num++]["parent_form"]=$(parent_form_item);
            }
            //the next code is for setting the checkboxes/radios hide/show options            
            if ($(parent_form_item).find(':radio,.form-checkboxes').size()){ //the next code is for setting the radios/checkboxes groups hide/show options
              $(parent_form_item).find(':radio,:checkbox').each(function (){//go through every radio/checkbox button in the parent form-item                    
                 var parent_checked=$(this).attr('checked')?1:0;            //get the state of the radio/checkbox
                 var _button=$(this);                 
                 var txt='option_text='+$(_button).val();
                 var parent_id=($(parent_form_item).attr('parent_num'));
                 $(_button).attr('parent_num',parent_id);                     //set the parent number of this checkbox/radio button                 
                 var $childs = $('div.child_of_'+field_hidden_text);          //find all child elements of this form-item
                 $childs.each(function(){                                     //go through all the dependent elements and hide/show them according to the state of the radio/checkbox button                          
                    if ($(this).html().split('option_text=')[1]==$(_button).val()){
                    //this specific checkbox/radio button is the parent of this field
                      var new_row={"form_item":$(parent_form_item),"parent":$(_button), "child":get_prev_form_item($(this))};
                      var in_array=$(this).attr('in_array');
                      if ((in_array===null)||(typeof in_array=="undefined")){//these 'if' makes sure that there are no duplicates in the table                        
                        var parent_num_attr=$(parent_form_item).attr('parent_num');                                             
                        parent_forms[parent_id]["children_arr"].push(get_prev_form_item($(this)));
                        parent_forms[parent_id]["parents_arr"].push($(_button));
                        parent_forms[parent_id]["parent_form"].attr('is_selection',0);
                        table[table_count++]=new_row;
                        $(this).attr('in_array',1);                        
                        if ($(this).html().indexOf('put_after_parent')>0){
                        //children that need to be put after their parents are only moved after the table is complete, so they are being signed for future recognition
                           get_prev_form_item($(this)).attr('put_after_parent',1);
                        }                              
                      }
                    }                    
                 });                        
                 $(this).click(function(){//whenever a click event occurs - go through all radio buttons, check their state and update the document 
                                          //this is important because in radio buttons choosing one option sets the previously selected radio button as unselected
                    var click_parent_num=$(this).attr('parent_num');                                                                  
                    if ((click_parent_num!==null)&&(typeof click_parent_num!="undefined")) {
                      var parentNum=parseInt($(this).attr('parent_num'));
                      show_hide_element(parent_forms[parseInt($(this).attr('parent_num'))]["parent_form"]);                                            
                    }
                 });
              });                     
            }
            else if ($(parent_form_item).find(':checkbox').size()){//the form item input is from type single on/off checkbox                                     
              var parent_checked=$(parent_form_item).find(':checkbox').attr('checked')?1:0;//get the check state of the checkbox                                                
              $('div.child_of_'+field_hidden_text).each(function(){              
                var _button=$(parent_form_item).find(':checkbox')
                var new_row={"form_item":$(parent_form_item),"parent":$(_button), "child":get_prev_form_item($(this))};
                var in_array=$(this).attr('in_array');
                if ((in_array===null)||(typeof in_array=="undefined")){                
                  var parent_id=($(parent_form_item).attr('parent_num'));
                  $(_button).attr('parent_num',parent_id);
                  parent_forms[parent_id]["children_arr"].push(get_prev_form_item($(this)));
                  parent_forms[parent_id]["parents_arr"].push($(_button));
                  parent_forms[parent_id]["parent_form"].attr('is_selection',0);
                  table[table_count++]=new_row;
                  $(this).attr('in_array',1);                  
                  if ($(this).html().indexOf('put_after_parent')>0){
                    get_prev_form_item($(this)).attr('put_after_parent',1);
                  }                             
                }
              });                    
              $(parent_form_item).find(':checkbox').click(function(){//set the click event of the checkbox                                     
                show_hide_element(parent_forms[parseInt($(this).attr('parent_num'))]["parent_form"]);                 
            	});
            } 
            else if ($(parent_form_item).find('select').size()){
              var selection_form_item=$(this);
              var selection_field=$(parent_form_item).find('select');
              var selectedIndex=selection_field[0].selectedIndex;
              var options=selection_field[0].options;
              var $childs = $('div.child_of_'+field_hidden_text);          //find all child elements of this form-item
              $childs.each(function(){                                     //go through all the dependent elements and hide/show them according to the state of the radio/checkbox button                                          
                  var new_row={"form_item":$(parent_form_item),"parent":$(selection_field), "child":get_prev_form_item($(this))};
                  var in_array=$(this).attr('in_array');
                  var parent_id=($(parent_form_item).attr('parent_num')); 
                  if ((in_array===null)||(typeof in_array=="undefined")){//these 'if' makes sure that there are no duplicates in the table                        
                    var parent_num_attr=$(parent_form_item).attr('parent_num'); 
                    var child=get_prev_form_item($(this));                                            
                    parent_forms[parent_id]["children_arr"].push($(child));
                    parent_forms[parent_id]["parents_arr"].push($(selection_field));
                    parent_forms[parent_id]["parent_form"].attr('is_selection',1);
                    table[table_count++]=new_row;
                    $(this).attr('in_array',1);
                    var option_text=$(this).html().split('option_text=')[1];
                    var option_index;
                    for (var i=0; i<options.length; ++i){
                      if (options[i].text==option_text)
                        option_index=i;
                    }
                    $(child).attr('option_index',option_index);
                    $(selection_field).attr('parent_num',parent_id);                        
                    if ($(this).html().indexOf('put_after_parent')>0){
                    //children that need to be put after their parents are only moved after the table is complete, so they are being signed for future recognition
                       get_prev_form_item($(this)).attr('put_after_parent',1);
                    }                              
                  }
                });
                $(selection_field).change(function(){
                  show_hide_element(parent_forms[parseInt($(this).attr('parent_num'))]["parent_form"]);                 
                });                    
              } 
              //debugger;  
           // }                  
          });
      });
      $('#autosave-status #view a').click(function() {
        show_hide_elements();
      });   
      arrange_children();      
      show_hide_elements();                              	
    });                        
   // }); 
     
function hide_show_childs_by_element(childElement,checked, parent){    
//this function gets a sign element, a button state and the parent of the dependent element
//in order to get the dependent element get_prev_form_item is used
//the parent element is used in order to hide/show the dependent element accroding to the visibility of the parent
//if the parent is hidden the dependent elements should also be hidden.        
      
    if (!parent)
      return;
    
    //get the visivility of the parent, if not set then set it as "true"
    var parent_visible=$(parent).attr('_visible');
    if ((parent_visible===null)||(typeof parent_visible=="undefined")){
      $(parent).attr('_visible',"true");
      parent_visible="true"; 
    }
    
    //get the child element and check its visibility. if not set then set it and all 
    //its children(important because radio-button are form-item children of a parent form-item, the latter is the element that actually being shown/hidden) 
    //form-item elements as true
    //var childElement=get_prev_form_item($(childSignElement));//maybe should be in each clause
    var child_visible= $(childElement).attr('_visible');
    var childElementFormItems=$(childElement).find('div.form-item');
    if ((child_visible===null)||(typeof child_visible=="undefined")){
      $(childElementFormItems).attr('_visible',"true");
      $(childElement).attr('_visible',"true");
      child_visible="true"; 
    } 
    var change=0;    
    //show/hide the child element and update its visible state
    //aftert the update call hide_show_elements in order to update its own dependent elements 
    var firstChild=$(childElement).find(":first");    
    var firstChildIsFieldset= firstChild.is("fieldset"); //this means these form item is a related subform element
    
    if ((checked)&&(child_visible=="false")&&(parent_visible=="true")){
      $(childElement).show('slow');
      if (!firstChildIsFieldset)
        $(childElementFormItems).attr('_visible',"true");
      $(childElement).attr('_visible',"true");      
      change=1;
    }
    else if (((!checked)||(parent_visible=="false"))&&(child_visible=="true")){
      $(childElement).hide('slow');
      if (!firstChildIsFieldset)
        $(childElementFormItems).attr('_visible',"false");
      $(childElement).attr('_visible',"false");      
      change=1;
    }    
    if ((change==1)&&(!firstChildIsFieldset)){//
      show_hide_element($(childElement));
    }
}

function get_prev_form_item(element){
    while(!(($(element).is("div.form-item"))||($(element).is("fieldset")))){
      element=element.prev();      
    }
    return element;
} 


function get_parent_form_item(element){
    while(!(($(element).is("div.form-item"))||($(element).is("fieldset")))){
      element=element.parent();      
    }
    return element;
} 

function show_hide_elements(){
//this function is being called whenever a radio button is clicked or shown/hidden
//it iterates through all elements that depend on a radio button 
  for(var parent_form=1; parent_form<parent_forms.length; ++parent_form){    
    show_hide_element(parent_forms[parent_form]["parent_form"]);
  }
}

function show_hide_element(parent){
//this function is being called whenever a radio button is clicked or shown/hidden
//it iterates through all elements that depend on a radio button
  var parent_id=$(parent).attr('parent_num');
    if ((parent_id===null)||(typeof parent_id=="undefined")) 
      return;
    var parent_form=parent_forms[parseInt(parent_id)];
    var number_of_children=parent_form["children_arr"].length;
    for (var child=0; child<number_of_children; child++){
      var checked=0;
      //var selection_field=$(parent)
      var sdkjh=typeof ($(parent).attr('is_selection'));    
      if ($(parent).attr('is_selection') == 1){
        var selection_field=parent_form["parents_arr"][child];
        if (parent_form["children_arr"][child].attr('option_index')== selection_field[0].selectedIndex){
          checked=1;
        }
      }
      hide_show_childs_by_element(parent_form["children_arr"][child], parent_form["parents_arr"][child].attr('checked')?1:checked, parent_form["parent_form"]);   
    }  
}

function arrange_children(){   
  for (var index=0; index<table.length; index++){
    if (table[index]["child"].attr('put_after_parent')){
      table[index]["child"].appendTo(get_parent_form_item($(table[index]["parent"]).parent()));
      table[index]["child"].addClass("child"); 
    }    
  } 
}
